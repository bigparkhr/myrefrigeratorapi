package com.bg.myrefrigeratorapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
