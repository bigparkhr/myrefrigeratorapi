package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.refrigeratorfood.RefrigeratorFoodRequest;
import com.bg.myrefrigeratorapi.repository.RefrigeratorFoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class RefrigeratorFoodService {
    private final RefrigeratorFoodRepository refrigeratorFoodRepository;

    public void setRefrigeratorFood(RefrigeratorFoodRequest request){
        RefrigeratorFood addData = new RefrigeratorFood();
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setDateRefrigeratorFood(request.getDateRefrigeratorFood());

        refrigeratorFoodRepository.save(addData);
        System.out.print(addData);
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    public ListResults<RefrigeratorFood> getRefrigerators(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<RefrigeratorFood> refrigeratorFoods = refrigeratorFoodRepository.findAll(pageRequest);

        ListResults<RefrigeratorFood> result = new ListResults<>();
        result.setList(refrigeratorFoods.getContent());
        result.setTotalCount(refrigeratorFoods.getTotalElements());
        result.setTotalPage(refrigeratorFoods.getTotalPages());
        result.setCurrentPage(refrigeratorFoods.getPageable().getPageNumber());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}
