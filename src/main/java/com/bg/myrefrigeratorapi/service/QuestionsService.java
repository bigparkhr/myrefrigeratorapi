package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.question.QuestionResponse;
import com.bg.myrefrigeratorapi.model.question.QuestionStatusChangeRequest;
import com.bg.myrefrigeratorapi.model.question.QuestionsItem;
import com.bg.myrefrigeratorapi.model.question.QuestionsRequest;
import com.bg.myrefrigeratorapi.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionsService {
    private final QuestionRepository questionRepository;

    public Questions getDataIs(long id) {return questionRepository.findById(id).orElseThrow();}

    public void setQuestion(Member member, QuestionsRequest request){
        Questions addData = new Questions();

        addData.setMemberEntityId(member);
        addData.setQuestionCategory(request.getQuestionCategory());
        addData.setTitle(request.getTitle());
        addData.setQuestionContents(request.getQuestionContents());
        addData.setAnswerStatus(false);
        addData.setDateWrite(LocalDate.now());
        addData.setIsDelete(request.getIsDelete());
        addData.setDateDelete(request.getDateDelete());

        questionRepository.save(addData);
    }
    public List<QuestionsItem> getQuestions() {
        List<Questions> originList = questionRepository.findAllByOrderByIdAsc();

        List<QuestionsItem> result = new LinkedList<>();

        for (Questions questions : originList) {
            QuestionsItem addItem = new QuestionsItem();
            addItem.setId(questions.getId());
            addItem.setMemberEntityId(questions.getMemberEntityId().getMemberId());
            addItem.setQuestionCategory(questions.getQuestionCategory());
            addItem.setTitle(questions.getTitle());
            addItem.setQuestionContents(questions.getQuestionContents());
            addItem.setAnswerStatus(questions.getAnswerStatus() ? "완료" : "대기");
            addItem.setDateWrite(questions.getDateWrite());

            result.add(addItem);
        }
        return result;
    }
    public QuestionResponse getQuestion(long id) {
        Questions originData = questionRepository.findById(id).orElseThrow();

        QuestionResponse response = new QuestionResponse();
        response.setId(originData.getId());
        response.setMemberEntityId(originData.getMemberEntityId());
        response.setQuestionCategory(originData.getQuestionCategory());
        response.setTitle(originData.getTitle());
        response.setQuestionContents(originData.getQuestionContents());
        response.setAnswerStatusName(originData.getAnswerStatus() ? "완료" : "대기");
        response.setDateWrite(originData.getDateWrite());
        response.setIsDelete(originData.getIsDelete());
        response.setDateDelete(originData.getDateDelete());

        return response;
    }
    public void putQuestionStatus(long id, QuestionStatusChangeRequest request) {
        Questions originData = questionRepository.findById(id).orElseThrow();

        originData.setQuestionCategory(request.getQuestionCategory());
        originData.setTitle(request.getTitle());
        originData.setQuestionContents(request.getQuestionContents());
        originData.setAnswerStatus(request.getAnswerStatus());

        questionRepository.save(originData);
    }
    public void delQuestion(long id) {
        questionRepository.deleteById(id);
    }

    public ListResults<Questions> getQuestions(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 5);
        Page<Questions> questions = questionRepository.findAll(pageRequest);

        ListResults<Questions> result = new ListResults<>();
        result.setList(questions.getContent());
        result.setTotalCount(questions.getTotalElements());
        result.setTotalPage(questions.getTotalPages());
        result.setCurrentPage(questions.getPageable().getPageNumber());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}
