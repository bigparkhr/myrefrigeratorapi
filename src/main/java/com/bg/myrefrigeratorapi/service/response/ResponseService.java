package com.bg.myrefrigeratorapi.service.response;

import com.bg.myrefrigeratorapi.enums.code.ResultCode;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(result.getMsg());
        return result;
    }

    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result);
        return result;
    }

    public static void setSuccessResult(CommonResult result){
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    public static void setFailResult(CommonResult result){
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILURE.getCode());
        result.setMsg(ResultCode.FAILURE.getMsg());
    }
}
