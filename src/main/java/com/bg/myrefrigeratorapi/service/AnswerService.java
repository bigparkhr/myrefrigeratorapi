package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Answer;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.answer.AnswerItem;
import com.bg.myrefrigeratorapi.model.answer.AnswerRequest;
import com.bg.myrefrigeratorapi.model.answer.AnswerResponse;
import com.bg.myrefrigeratorapi.model.answer.AnswerStatusChangeRequest;
import com.bg.myrefrigeratorapi.repository.AnswerRepository;
import com.bg.myrefrigeratorapi.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;

   public void setAnswer(Questions questions , AnswerRequest request) {
       Answer addData = new Answer();
       addData.setQuestions(questions);
       addData.setAnswerTitle(request.getAnswerTitle());
       addData.setAnswer(request.getAnswer());
       addData.setDateAnswer(LocalDate.now());

       answerRepository.save(addData);

       /**
        * 답변 등록 했을때 답변 상태 변경
        */
       Questions originData = questionRepository.findById(questions.getId()).orElseThrow();

       originData.setAnswerStatus(true);

       questionRepository.save(originData);
   }
   public List<AnswerItem> getAnswers() {
       List<Answer> originList = answerRepository.findAll();

       List<AnswerItem> result = new LinkedList<>();

       for (Answer answer : originList) {
           AnswerItem addItem = new AnswerItem();
           addItem.setId(answer.getId());
           addItem.setQuestionsId(answer.getQuestions().getMemberEntityId().getMemberId());
           addItem.setAnswerTitle(answer.getAnswerTitle());
           addItem.setAnswer(answer.getAnswer());
           addItem.setDateAnswer(answer.getDateAnswer());

           result.add(addItem);
       }
       return result;
   }
   public AnswerResponse getAnswer(long id) {
       Answer originList = answerRepository.findById(id).orElseThrow();

       AnswerResponse response = new AnswerResponse();
       response.setId(originList.getQuestions().getId());
       response.setQuestionsId(originList.getQuestions().getId());
       response.setAnswerTitle(originList.getAnswerTitle());
       response.setAnswer(originList.getAnswer());
       response.setDateAnswer(originList.getDateAnswer());

       return response;
   }
   public void putAnswer(long id, AnswerStatusChangeRequest request) {
       Answer originData = answerRepository.findById(id).orElseThrow();

       originData.setAnswerTitle(request.getAnswerTitle());
       originData.setAnswer(request.getAnswer());

       answerRepository.save(originData);
   }
   public void delAnswer(long id) {
       answerRepository.deleteById(id);
   }

    /**
     * Paging
     * @param pageNum
     * @return
     */
   public ListResults<Answer> getAnswers(int pageNum) {
       PageRequest pageRequest = PageRequest.of(pageNum - 1, 5);
       Page<Answer> answers = answerRepository.findAll(pageRequest);

       ListResults<Answer> result = new ListResults<>();
       result.setList(answers.getContent());
       result.setTotalCount(answers.getTotalElements());
       result.setTotalPage(answers.getTotalPages());
       result.setCurrentPage(answers.getPageable().getPageNumber());
       result.setMsg("성공하였습니다.");
       result.setCode(0);

       return result;
   }
}