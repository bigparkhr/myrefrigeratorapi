package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.enums.MemberGroup;
import com.bg.myrefrigeratorapi.exception.CRefrigeratorFullException;
import com.bg.myrefrigeratorapi.lib.ComonCheck;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.member.*;
import com.bg.myrefrigeratorapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    // 관리자는  관리자만 등록가능해야하고 일반회원은 아무나 등록 가능해야한다.
    // 아무나 관리자로 등록가능하면..?
    // 그러니까 컨트롤러에서 일반회원가입, 관리자 등록 이렇게 두개로 나눠서 구현해야한다.
    // 근데 일반회원가입 서비스 따로, 관리자 등록 서비스 따로 이렇게 하면
    // 어차피 비슷한 코드인데 굳이 이걸 왜 나누냐?/
    // 그래서 어떤 회원그룹에 가입시킬껀지 서비스에서 받고

    public Member getData (long id){return memberRepository.findById(id).orElseThrow();}

    public MemberIdDupCheckResponse getDupCheck (String memberId){
        MemberIdDupCheckResponse response = new MemberIdDupCheckResponse();
        response.setIsNew(isMemberIdDupCheck(memberId));
        return response;
    }

    public void createMember(MemberGroup memberGroup, MemberRequest request) {
        if (!ComonCheck.checkUsername(request.getUsername())) throw new CRefrigeratorFullException(); // 유효한 아이디 형식이 아닙니다 전지
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CRefrigeratorFullException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isMemberIdDupCheck(request.getUsername())) throw new CRefrigeratorFullException(); // 중복된 아이디가 존재합니다 던지기

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Member member = new Member.Builder(request, memberGroup).build();

        memberRepository.save(member);
    }

    /**
     * 회원가입
     * @param //request
     * @throws Exception
     */
    /*public void setMember(MemberRequest request)throws Exception{
        if (!isMemberIdDupCheck(request.getMemberId())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member addData = new Member();

        addData.setMemberName(request.getMemberName());
        addData.setMemberId(request.getMemberId());
        addData.setInterlockInfo(request.getInterlockInfo());
        addData.setPassword(request.getPassword());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMail(request.getMail());
        addData.setDateJoin(LocalDate.now());
        addData.setIsOut(false);
        addData.setDateOut(request.getDateOut());

        memberRepository.save(addData);
    }*/

   /* public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAllByOrderByIdAsc();

        List<MemberItem> result = new LinkedList<>();

        for (Member member: originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setMemberName(member.getMemberName());
            addItem.setMemberId(member.getMemberId());
            addItem.setInterlockInfo(member.getInterlockInfo());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setMail(member.getMail());
            addItem.setDateJoin(member.getDateJoin());

            result.add(addItem);
        }
        return result;
    }*/
  /*  public MemberResponse getMember(long id) {
        Member originList = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originList.getId());
        response.setMemberName(originList.getMemberName());
        response.setMemberId(originList.getMemberId());
        response.setInterlockInfo(originList.getInterlockInfo());
        response.setPassword(originList.getPassword());
        response.setPhoneNumber(originList.getPhoneNumber());
        response.setMail(originList.getMail());
        response.setDateJoin(originList.getDateJoin());
        response.setIsOut(originList.getIsOut());
        response.setDateOut(originList.getDateOut());

        return response;
    }*/

    /*public void putMemberStatus(long id, MemeberStatusChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();

        originData.setMemberName(request.getMemberName());
        originData.setMemberId(request.getMemberId());
        originData.setPassword(request.getPassword());
        originData.setPhoneNumber(request.getPhoneNumber());
        originData.setMail(request.getMail());
        originData.setDateJoin(request.getDateJoin());

        memberRepository.save(originData);
    }*/

    /**
     * 멤버 탈퇴 시 삭제처럼 보이는 수정 기능
     * @param id
     */
    /*public void putMemberOut(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();

        String del = "del";

        originData.setMemberId(originData.getId()+del);
        originData.setMemberName(originData.getMemberName());
        originData.setMemberId(originData.getMemberId());
        originData.setInterlockInfo(originData.getInterlockInfo());
        originData.setPassword(originData.getPassword());
        originData.setPhoneNumber(originData.getPhoneNumber());
        originData.setMail(originData.getMail());
        originData.setDateJoin(originData.getDateJoin());
        originData.setIsOut(true);
        originData.setDateOut(originData.getDateOut());

        memberRepository.save(originData);
    }
*/
    /**
     * Paging
     * @param pageNum
     * @return
     */
    public ListResults<Member> getMembers(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 5);
        Page<Member> members = memberRepository.findAll(pageRequest);

        ListResults<Member> result = new ListResults<>();
        result.setList(members.getContent());
        result.setTotalCount(members.getTotalElements());
        result.setTotalPage(members.getTotalPages());
        result.setCurrentPage(members.getPageable().getPageNumber());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }

    private boolean isMemberIdDupCheck(String memberId){
        long dupCheck = memberRepository.countByMemberIdOrderByIdDesc(memberId);
        return dupCheck <= 0;
    }



}
