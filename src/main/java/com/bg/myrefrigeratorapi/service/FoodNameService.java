package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameItem;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameRequest;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameResponse;
import com.bg.myrefrigeratorapi.repository.FoodNameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodNameService {
    private final FoodNameRepository foodNameRepository;

    public FoodName getData(long id) {
        return foodNameRepository.findById(id).orElseThrow();
    }

    public void setFoodName(FoodNameRequest request) {
        FoodName addData = new FoodName();
        addData.setFoodName(request.getFoodName());
        addData.setRecipeImage(request.getRecipeImage());
        addData.setCalorie(request.getCalorie());
        addData.setDateRegister(LocalDate.now());
        addData.setIsRecipeDelete(false);
        addData.setDateRecipeDelete(LocalDate.now());

        foodNameRepository.save(addData);
    }
    public List<FoodNameItem> getFoodNames() {
        List<FoodName> originList = foodNameRepository.findAll();

        List<FoodNameItem> result = new LinkedList<>();

        for (FoodName foodName : originList) {
            FoodNameItem addITem = new FoodNameItem();
            addITem.setId(foodName.getId());
            addITem.setFoodName(foodName.getFoodName());
            addITem.setRecipeImage(foodName.getRecipeImage());
            addITem.setCalorie(foodName.getCalorie());
            addITem.setDateRegister(foodName.getDateRegister());

            result.add(addITem);
        }
        return result;
    }
    public FoodNameResponse getFoodName(long id) {
        FoodName originList = foodNameRepository.findById(id).orElseThrow();

        FoodNameResponse response = new FoodNameResponse();
        response.setId(originList.getId());
        response.setFoodName(originList.getFoodName());
        response.setRecipeImage(originList.getRecipeImage());
        response.setCalorie(originList.getCalorie());
        response.setDateRegister(originList.getDateRegister());
        response.setIsRecipeDelete(originList.getIsRecipeDelete());
        response.setDateRecipeDelete(originList.getDateRecipeDelete());

        return response;
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    public ListResults<FoodName> getFoodNames(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 5);
        Page<FoodName> foodNames = foodNameRepository.findAll(pageRequest);

        ListResults<FoodName> result = new ListResults<>();
        result.setList(foodNames.getContent());
        result.setTotalCount(foodNames.getTotalElements());
        result.setTotalPage(foodNames.getTotalPages());
        result.setCurrentPage(foodNames.getPageable().getPageNumber());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}
