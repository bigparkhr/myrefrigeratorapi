package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodIngredient;
import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientItem;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientRequest;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientResponse;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientSearch;
import com.bg.myrefrigeratorapi.repository.FoodIngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class FoodIngredientService {
    private final FoodIngredientRepository foodIngredientRepository;

    public void setFoodIngredient(FoodName foodName, FoodIngredientRequest request) {
        FoodIngredient addData = new FoodIngredient();
        addData.setFoodName(foodName);
        addData.setCategory(request.getCategory());
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setCapacity(request.getCapacity());

        foodIngredientRepository.save(addData);
    }

    public List<FoodIngredientItem> getFoodIngredients() {
        List<FoodIngredient> originList = foodIngredientRepository.findAll();

        List<FoodIngredientItem> result = new LinkedList<>();

        for (FoodIngredient foodIngredient : originList) {
            FoodIngredientItem addItem = new FoodIngredientItem();
            addItem.setId(foodIngredient.getId());
            addItem.setFoodName(foodIngredient.getFoodName().getId());
            addItem.setCategory(foodIngredient.getCategory());
            addItem.setRefrigeratorFood(foodIngredient.getRefrigeratorFood().getId());
            addItem.setCapacity(foodIngredient.getCapacity());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 식재료를 통한 검색 기능
     * @param
     * @return 중복 결과 데이터 제거
     */
    public List<FoodIngredientSearch> getSearch(long refrigeratorFood) {
        List<FoodIngredient> originList = foodIngredientRepository.findAllByRefrigeratorFoodId(refrigeratorFood);

        List<FoodIngredientSearch> result = new LinkedList<>();

        for (FoodIngredient foodIngredient : originList) {
            FoodIngredientSearch addItem = new FoodIngredientSearch();
            addItem.setFoodNameId(foodIngredient.getFoodName().getId());
            addItem.setFoodName(foodIngredient.getFoodName().getFoodName());
            addItem.setRefrigeratorFood(foodIngredient.getRefrigeratorFood().getRefrigeratorFood());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 음식명Id에 관한 재료 목록 불러오기
     * @param foodName
     * @return
     */
    public List<FoodIngredientResponse> getFoodIngredient(FoodName foodName) {
        List<FoodIngredient> originList = foodIngredientRepository.findByFoodName(foodName);

        List<FoodIngredientResponse> result = new LinkedList<>();

        for (FoodIngredient foodIngredient : originList) {
            FoodIngredientResponse response = new FoodIngredientResponse();
            response.setRefrigeratorFood(foodIngredient.getRefrigeratorFood().getRefrigeratorFood());

            result.add(response);
        }
        return result;
    }
}
