package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Advertising;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingItem;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingRequest;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingResponse;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingStatusChangeRequest;
import com.bg.myrefrigeratorapi.repository.AdvertisingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AdvertisingService {
    private final AdvertisingRepository advertisingRepository;

    public void setAdvertising (AdvertisingRequest request){
        Advertising addData = new Advertising();
        addData.setAdvertisinompany(request.getAdvertisinompany());
        addData.setPaymentType(request.getPaymentType());
        addData.setPaymentAmount(request.getPaymentAmount());
        addData.setDatePayment(request.getDatePayment());
        addData.setContractStartDate(request.getContractStartDate());
        addData.setEndDateOfContract(request.getEndDateOfContract());
        addData.setIsExpiration(false);
        addData.setRequested(request.getRequested());

        advertisingRepository.save(addData);
    }
    public List<AdvertisingItem> getAdvertisings() {
        List<Advertising> originList = advertisingRepository.findAll();

        List<AdvertisingItem> result = new LinkedList<>();

        for (Advertising advertising : originList) {
            AdvertisingItem addItem = new AdvertisingItem();
            addItem.setId(advertising.getId());
            addItem.setAdvertisinompany(advertising.getAdvertisinompany());
            addItem.setPaymentType(advertising.getPaymentType());
            addItem.setPaymentAmount(advertising.getPaymentAmount());
            addItem.setDatePayment(advertising.getDatePayment());
            addItem.setContractStartDate(advertising.getContractStartDate());
            addItem.setEndDateOfContract(advertising.getEndDateOfContract());
            addItem.setIsExpiration(false);

            result.add(addItem);
        }
        return result;
    }
    public AdvertisingResponse getAdvertising(long id) {
        Advertising originList = advertisingRepository.findById(id).orElseThrow();

        AdvertisingResponse response = new AdvertisingResponse();
        response.setId(originList.getId());
        response.setAdvertisinompany(originList.getAdvertisinompany());
        response.setPaymentType(originList.getPaymentType());
        response.setPaymentAmount(originList.getPaymentAmount());
        response.setDatePayment(originList.getDatePayment());
        response.setContractStartDate(originList.getContractStartDate());
        response.setEndDateOfContract(originList.getEndDateOfContract());
        response.setIsExpiration(originList.getIsExpiration());
        response.setRequested(originList.getRequested());

        return response;
    }
    public void putAdvertising(long id, AdvertisingStatusChangeRequest request) {
        Advertising originData = advertisingRepository.findById(id).orElseThrow();

        originData.setAdvertisinompany(request.getAdvertisinompany());
        originData.setPaymentType(request.getPaymentType());
        originData.setEndDateOfContract(request.getEndDateOfContract());
        originData.setRequested(request.getRequested());

        advertisingRepository.save(originData);
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    public ListResults<Advertising> getAdvertisings(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 5);
        Page<Advertising> advertisings = advertisingRepository.findAll(pageRequest);

        ListResults<Advertising> result = new ListResults<>();
        result.setList(advertisings.getContent());
        result.setTotalCount(advertisings.getTotalElements());
        result.setTotalPage(advertisings.getTotalPages());
        result.setCurrentPage(advertisings.getPageable().getPageNumber());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}

