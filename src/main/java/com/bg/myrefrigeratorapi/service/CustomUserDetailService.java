package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.exception.CRefrigeratorFullException;
import com.bg.myrefrigeratorapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(CRefrigeratorFullException::new);

        return member;
    }
}
