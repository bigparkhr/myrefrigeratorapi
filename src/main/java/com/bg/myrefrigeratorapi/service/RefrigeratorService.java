package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorItem;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorRequest;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorStatusChangeRequest;
import com.bg.myrefrigeratorapi.repository.RefrigeratorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RefrigeratorService {
    private final RefrigeratorRepository refrigeratorRepository;

    public Refrigerator getData(long id) {return refrigeratorRepository.findById(id).orElseThrow();}

    public void setRefrigerator(Member member, RefrigeratorRequest request){
        Refrigerator addData = new Refrigerator();
        addData.setMember(member);
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setDateRefrigerator(LocalDate.now());
        addData.setRefrigeratorName(request.getRefrigeratorName());

        refrigeratorRepository.save(addData);
    }
    public List<RefrigeratorItem> getRefrigerators() {
        List<Refrigerator> originList = refrigeratorRepository.findAll();

        List<RefrigeratorItem> result = new LinkedList<>();

        for (Refrigerator refrigerator : originList) {
            RefrigeratorItem addItem = new RefrigeratorItem();
            addItem.setId(refrigerator.getId());
            addItem.setMemberId(refrigerator.getMember().getId());
            addItem.setRefrigeratorFood(refrigerator.getRefrigeratorFood().getRefrigeratorFood());
            addItem.setDateRefrigerator(LocalDate.now());
            addItem.setRefrigeratorName(refrigerator.getRefrigeratorName());

            result.add(addItem);
        }
        return result;
    }
    public List<RefrigeratorItem> getRefrigerator(Member member) {
        List<Refrigerator> originList = refrigeratorRepository.findByMember(member);

        List<RefrigeratorItem> result = new LinkedList<>();

        for (Refrigerator refrigerator : originList) {
            RefrigeratorItem addItem = new RefrigeratorItem();
            addItem.setId(refrigerator.getId());
            addItem.setMemberId(refrigerator.getMember().getId());
            addItem.setDateRefrigerator(refrigerator.getDateRefrigerator());
            addItem.setRefrigeratorName(refrigerator.getRefrigeratorName());
            addItem.setRefrigeratorFood(refrigerator.getRefrigeratorFood().getRefrigeratorFood());

            result.add(addItem);
        }
        return result;
    }
    public void putRefrigerator(long id, RefrigeratorStatusChangeRequest request) {
        Refrigerator originData = refrigeratorRepository.findById(id).orElseThrow();

        originData.setRefrigeratorFood(request.getRefrigeratorFood());

        refrigeratorRepository.save(originData);
    }
    /**
     * Paging
     */
    public ListResults<Refrigerator> getRefrigerators(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 5);
        Page<Refrigerator> refrigerators = refrigeratorRepository.findAll(pageRequest);

        ListResults<Refrigerator> result = new ListResults<>();
        result.setList(refrigerators.getContent());
        result.setTotalCount(refrigerators.getTotalElements());
        result.setTotalPage(refrigerators.getTotalPages());
        result.setCurrentPage(refrigerators.getTotalPages());
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}
