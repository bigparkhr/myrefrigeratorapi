package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Comment;
import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.model.comment.CommentDTO;
import com.bg.myrefrigeratorapi.model.comment.CommentItem;
import com.bg.myrefrigeratorapi.model.comment.CommentRequest;
import com.bg.myrefrigeratorapi.repository.CommentRepository;
import com.bg.myrefrigeratorapi.repository.FoodNameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    /**
     *레시피 댓글 등록
     * @param foodName
     * @param member
     * @param request
     */
    public void setComment(FoodName foodName, Member member, CommentRequest request) {
        Comment addData = new Comment();
        addData.setFoodName(foodName);
        addData.setMember(member);
        addData.setCommentContents(request.getCommentContents());
        addData.setCommentCreatedTime(LocalDateTime.now());

        commentRepository.save(addData);
    }

    /**
     * 댓글 리스트 불러오기
     * @param foodName
     * @return
     */
    public List<CommentItem> getComments(FoodName foodName) {
        List<Comment> originList = commentRepository.findAllByFoodNameOrderByIdAsc(foodName);

        List<CommentItem> result = new LinkedList<>();

        for (Comment comment : originList) {
            CommentItem addItem = new CommentItem();
            addItem.setId(comment.getId());
            addItem.setFoodName(comment.getFoodName().getFoodName());
            addItem.setMemberId(comment.getMember().getMemberId());
            addItem.setCommentContents(comment.getCommentContents());
            addItem.setCommentCreatedTime(LocalDateTime.now());

            result.add(addItem);
        }
        return result;
    }
}
