package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.OrderOfCooking;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingChangeRequest;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingItem;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingRequest;
import com.bg.myrefrigeratorapi.repository.OrderOfCookingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderOfCookingService {
    private final OrderOfCookingRepository orderOfCookingRepository;

    public void setOrderOfCooking(FoodName foodName, OrderOfCookingRequest request) {
        OrderOfCooking addData = new OrderOfCooking();
        addData.setFoodNameId(foodName);
        addData.setRecipe(request.getRecipe());

        orderOfCookingRepository.save(addData);
    }
    public List<OrderOfCookingItem> getOrderOfCookings() {
        List<OrderOfCooking> originList = orderOfCookingRepository.findAll();

        List<OrderOfCookingItem> result = new LinkedList<>();

        for (OrderOfCooking orderOfCooking : originList) {
            OrderOfCookingItem addItem = new OrderOfCookingItem();
            addItem.setId(orderOfCooking.getId());
            addItem.setFoodNameId(orderOfCooking.getFoodNameId().getId());
            addItem.setRecipe(orderOfCooking.getRecipe());

            result.add(addItem);
        }
        return result;
    }
    public List<OrderOfCookingItem> getFoodNameOrderOfCookings(FoodName foodName) {
        List<OrderOfCooking> originList = orderOfCookingRepository.findAllByFoodNameIdOrderByIdAsc(foodName);

        List<OrderOfCookingItem> result = new LinkedList<>();

        for (OrderOfCooking orderOfCooking : originList) {
            OrderOfCookingItem addItem = new OrderOfCookingItem();
            addItem.setId(orderOfCooking.getId());
            addItem.setFoodNameId(orderOfCooking.getFoodNameId().getId());
            addItem.setRecipe(orderOfCooking.getRecipe());

            result.add(addItem);
        }
        return result;
    }
    public void putOrderOfCooking(long id, OrderOfCookingChangeRequest request) {
        OrderOfCooking originData = orderOfCookingRepository.findById(id).orElseThrow();
        originData.setRecipe(request.getRecipe());

        orderOfCookingRepository.save(originData);
    }
    public void delOrderOFCooking(long id) {
        orderOfCookingRepository.deleteById(id);
    }
}
