package com.bg.myrefrigeratorapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    INGREDIENT("재료"),
    SEASONING("양념");

    private final String category;
}
