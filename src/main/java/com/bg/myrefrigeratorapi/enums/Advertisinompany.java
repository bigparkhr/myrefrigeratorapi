package com.bg.myrefrigeratorapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum Advertisinompany {

    COUPANG("쿠팡"),
    ELEVENTHSREET("11번가"),
    AUCTION("옥션"),
    GMARKET("G마켓"),
    LOTTEON("롯데온");

    private final String name;
}
