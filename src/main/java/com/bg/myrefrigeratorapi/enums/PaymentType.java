package com.bg.myrefrigeratorapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PaymentType {

    CARD("카드"),
    CASH("현금");

    private final String Payment;
}

