package com.bg.myrefrigeratorapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberGroup {
    ROLE_CEO("사장"),
    ROLE_MEMBER("사원");

    private final String grade;
}
