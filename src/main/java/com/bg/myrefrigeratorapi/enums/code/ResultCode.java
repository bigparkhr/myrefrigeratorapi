package com.bg.myrefrigeratorapi.enums.code;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다"),
    FAILURE(-1, "실패하였습니다"),
    REGISTER(0,"성공적으로 등록하였습니다"),
    ALL(0, "리스트를 성공적으로 불러왔습니다"),
    DETAIL(0,"상세보기를 성공적으로 불러왔습니다"),
    CHANGE(0, "성공적으로 수정하였습니다"),
    DELETE(0,"성공적으로 삭제하였습니다"),
    MEMBER_MISSING(-1000,"멤버의 정보가 없습니다.");

    // 공통적인거는 여기서 불러오고

    private final Integer code;
    private final String msg;
}
