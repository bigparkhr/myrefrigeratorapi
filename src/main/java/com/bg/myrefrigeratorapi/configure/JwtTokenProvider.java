package com.bg.myrefrigeratorapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    @PostConstruct // 먼저 키를 암호화함
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    // 토큰 생성
    // claim 이라는 단어 뜻 찾기 -> 주장하다 무엇을 나라는걸 주장함
    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username); // 클레임은 JWT에 포함될 정보를 나타내고 클레임의 주제(subject)를 사용자 이름으로 설정
        claims.put("role", role); // 사용자의 역할
        Date now = new Date(); // 토큰의 발급 시간으로 설정

        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본을 10시간 유효하게 설정해줌 왜냐하면 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대충 10시간이니깐
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해줌. 앱에서 아침마다 로그인하라고 하면 싫으니깐
        long tokenValidMillisecond = 1000L * 60 * 60 * 10; // 10시간
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365; // 1년 웹 토큰은 쿠키도 시간을 동일하게 맞춰야함

        // 토큰 생성해서 리턴
        // jwt 사이트 참고
        // 유효시간도 넣어줌. 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼.
        return Jwts.builder() // 토큰을 찍어내는 클래스
                .setClaims(claims) // 나야라고 명시
                .setIssuedAt(now) // 언제 발급됐는지
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond)) // 언제 얘가 파기되는지 (만료일)
                .signWith(SignatureAlgorithm.HS256, secretKey) // 사인이 없으면 위조하기 때문에 시크릿키를 준비
                .compact(); // 토큰이 작기 때문에 콤팩트하게 작업

        // 토큰은 스트링으로 생김
    }

    // 토큰을 분석하여 인증정보를 가져옴
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    } // 프로바이더는 제공자, super(authorities); = userDetails, principal = "", setAuthenticated(true) = userDetails.getAuthorities
      // 부모한테 권한을 받아서 체크한 다음 부모한테 인증 정보를 만들어달라고 함
      // Authentication = 1회용 출입증

    // 토큰을 파싱하여 username을 가져옴
    // 토큰 생성시 username은 subject에 넣은 것 꼭 확인
    // jwt 사이트 보면서 코드 이해하기.
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    // resolve라는 단어 뜻 검색하기 -> 결정, 해결
    public String resolveToken(HttpServletRequest request) {
        // rest api - header 인증 방식에서 Bearer 를 언제 사용하는지 보기
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    } // 성공이든 실패든 전부 프로미스한거고 리졸브는 약속중에 성공한것만을 뜻함 성공값만 받는것이 리졸브
      // 프로미스 - 뭔가를 주기로 한것이 API 콜 우선 약속을 했으니 성공하든 실패하든 무조건 오는 것
      // 성공한 토큰을 갖고 온다는 것 헤더에서 토큰값만 갖고오는 것 그래서 이름이 get이 아니라 resolve

    // 유효시간을 검사하여 유효시간이 지났으면 false를 줌
    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    } // 여기서 왜 던지지 않았냐면 트라이 캐치의 단점은 틀려도 실행이 되니 트로우를 던져야 끝나고 여기서 던지면 유지보수의 문제가 생김 이것저것 다 던지기 시작하면 에러를 찾을 수 없음
      //
}