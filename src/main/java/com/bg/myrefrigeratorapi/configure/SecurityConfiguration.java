package com.bg.myrefrigeratorapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(List.of("*")); //
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS")); // OPTION는 api 호출할 때 한 요청당 2번씩 가고 프리프라이트에 맵칭이 옵션
        config.setAllowedHeaders(List.of("*")); // 모든 헤더 요청 받는것
        config.setExposedHeaders(List.of("*")); // 노출된 헤더 모두 허용

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config); // /** = 모든 맵핑 주소에 대해서 모든 설정을 넘기겠다는 것
        return source;
    }

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception { // 보안팀한테 위임하는 과정
        http.csrf(AbstractHttpConfigurer::disable) // crsf를 비활성화
                .formLogin(AbstractHttpConfigurer::disable) // 폼 로그인 기능을 비활성화 이것은 사용자가 웹 페이지에서 폼을 통해 로그인할 때의 기능
                .httpBasic(AbstractHttpConfigurer::disable) // HTTP 기본 인증을 비활성화 HTTP 기본 인증은 사용자 이름과 비밀번호를 요청 헤더에 직접 포함하여 인증하는 방식
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                .sessionManagement((sessionManagement) -> // 위에 3개 땜에 스테이리스로 설정
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // 세션을 안 쓰기 때문에 STATELESS로 설정
                .authorizeHttpRequests((authorizeRequests) -> // 인가는 인증 결과를 허가하는 것, 데이터 요청에 대해서 인가해주는 것
                        authorizeRequests
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll() // springboot 3버전이어서 v3,
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll() // 먼저 뚫어줌
                                .requestMatchers("/v1/member/login/**").permitAll() // 멤버 로그인 모두 풀어줌
                                .requestMatchers("/v1/member/**").permitAll()
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("MEMBER", "ADMIN") // 이 주소에 대한 권한 설정
                                .anyRequest().hasAnyRole("ADMIN") // 그외 공간에 대한 권한 설정
                );

        // 트리거 순서 JwtTokenFilter을 먼저 통과해야함
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied)); // 에러 메세지를 핸들러에게 넘기는 과정
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class); // 비포 달린 이유는 첫번째로 실행해야해서
        // 클래스는 필터의 자리됨, 맨 처음에 프로바이더에게 회원 정보에 대해 문제가 있는 것은 해당 필터에 들어가게 설정

        return http.build(); // BUILD해서 SecurityFilterChain에게 넘겨줌
        // 책상위에서 작업하고 내보내는 형식이므로 따로 변수를 설정하지 않고 바로 리턴이 가능
    }
}
