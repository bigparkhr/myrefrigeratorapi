package com.bg.myrefrigeratorapi.configure;

import com.bg.myrefrigeratorapi.enums.code.ResultCode;
import com.bg.myrefrigeratorapi.exception.CRefrigeratorFullException;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILURE);
    }

    @ExceptionHandler(CRefrigeratorFullException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CRefrigeratorFullException e) {
        return ResponseService.getFailResult(ResultCode.FAILURE);
    }
}