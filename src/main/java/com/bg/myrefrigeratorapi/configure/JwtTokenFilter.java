package com.bg.myrefrigeratorapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends GenericFilterBean { // GenericFilterBean(필터 종류 어느것이든 빈(시작할때 설정값)에 저장) 기능을 상속
    private final JwtTokenProvider jwtTokenProvider;

    // 고객이 서버한테 주는 값, 서버가 사람한테 주는 값, 필터 단계를 설정
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) { // 받아오는 리졸브 값이 없어가 Bearer 이 없으면
            chain.doFilter(request, response); // 필터링할 필요도 없으니
            return; // void인데 return이 있으면 끝났다는 의미
        }
        token = tokenFull.split(" ")[1].trim(); // 띄어쓰기 기준으로 토큰값만 갖고온다음 첫번째 요소를 갖고오고 trim은 문자열 앞뒤 공백을 제거 혹시나 모를 에러 방지용
        if (!jwtTokenProvider.validateToken(token)) { // 제공자한테 토큰 검사해달라하고 유효한 토큰이 아니면 나가라고 함
            chain.doFilter(request, response);
            return;
        }
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication); // context는 환경 홀더는 고정시키기 위해 즉 1회용 출입증을 고정시키기 위해 출입증은 모든 기능이 써야해서 요청에 한해 잠시 고정하여 유실을 방지
        chain.doFilter(request, response);
    }
}