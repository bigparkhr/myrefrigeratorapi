package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.enums.Category;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class FoodIngredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "foodNameId", nullable = false)
    private FoodName foodName;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Category category;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "refrigeratorFoodId", nullable = false)
    private RefrigeratorFood refrigeratorFood;

    @Column(nullable = false, length = 10)
    private String capacity;
}
