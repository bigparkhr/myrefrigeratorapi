package com.bg.myrefrigeratorapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Refrigerator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "memberId")
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false , name = "refrigeratorFoodId")
    private RefrigeratorFood refrigeratorFood;

    @Column(nullable = false)
    private LocalDate dateRefrigerator;

    @Column(nullable = false, length = 10)
    private String RefrigeratorName;
}

