package com.bg.myrefrigeratorapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "questionsId", unique = true)
    private Questions questions;

    @Column(nullable = false, length = 20)
    private String AnswerTitle;

    @Column(nullable = false , columnDefinition = "TEXT")
    private String Answer;

    @Column(nullable = false)
    private LocalDate dateAnswer;
}
