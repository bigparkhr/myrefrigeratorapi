package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.model.comment.CommentDTO;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column
    private String commentContents;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "foodNameId", nullable = false)
    private FoodName foodName;

    private LocalDateTime commentCreatedTime;

    /**
     * toSave 메소드
     * @param commentDTO
     * @param foodName
     * @return
     */
    public static Comment toSave(CommentDTO commentDTO, FoodName foodName) {
        Comment comment = new Comment();
        comment.setMember(commentDTO.getMember());
        comment.setCommentContents(commentDTO.getCommentContents());
        comment.setFoodName(foodName);

        return comment;
    }
}
