package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.enums.InterLockInfo;
import com.bg.myrefrigeratorapi.enums.MemberGroup;
import com.bg.myrefrigeratorapi.interfaces.CommonModelBuilder;
import com.bg.myrefrigeratorapi.model.member.MemberRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false , length = 20)
    private String username;

    @Column(nullable = false , length = 20, unique = true)
    private String memberId;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberGroup memberGroup;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false , length = 20)
    private InterLockInfo interlockInfo;

    @Column(nullable = false , length = 20, unique = true)
    private String password;

    @Column(nullable = false , length = 13)
    private String phoneNumber;

    @Column(nullable = false , length = 30)
    private String mail;

    @Column(nullable = false)
    private LocalDate dateJoin;

    @Column(nullable = false)
    private Boolean isOut = Boolean.FALSE;

    @Column
    private LocalDate dateOut;

    private Member(Builder builder) {
        this.username = builder.username;
        this.memberId = builder.memberId;
        this.interlockInfo = builder.interLockInfo;
        this.password = builder.password;

        this.phoneNumber = builder.phoneNumber;
        this.mail = builder.mail;
        this.dateJoin = builder.dateJoin;
        this.isOut = builder.isOut;
        this.dateOut = builder.dateOut;
        this.memberGroup = builder.memberGroup;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public String getUsername() {
        return memberId;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final String username;
        private final String memberId;
        private final InterLockInfo interLockInfo;
        private final String password;
        private final String phoneNumber;
        private final String mail;
        private final LocalDate dateJoin;
        private final Boolean isOut;
        private final LocalDate dateOut;
        private final MemberGroup memberGroup;


        public Builder(MemberRequest request, MemberGroup memberGroup){
            this.username = request.getUsername();
            this.memberId = request.getMemberId();
            this.interLockInfo = request.getInterlockInfo();
            this.password = request.getPassword();
            this.phoneNumber = request.getPhoneNumber();
            this.mail = request.getMail();
            this.dateJoin = LocalDate.now();
            this.isOut = request.getIsOut();
            this.dateOut = request.getDateOut();
            this.memberGroup = memberGroup;
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }

}
