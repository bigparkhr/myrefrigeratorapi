package com.bg.myrefrigeratorapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class FoodName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20, unique = true)
    private String foodName;

    @Column(columnDefinition = "TEXT", unique = true)
    private String recipeImage;

    @Column(nullable = false, length = 10)
    private String calorie;

    @Column(nullable = false)
    private LocalDate dateRegister;

    @Column(nullable = false)
    private Boolean isRecipeDelete;

    private LocalDate dateRecipeDelete;

    /**
     * 댓글 리스트 불러오기
     */
    @OneToMany(mappedBy = "foodName", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Comment> commentList = new ArrayList<>();
}
