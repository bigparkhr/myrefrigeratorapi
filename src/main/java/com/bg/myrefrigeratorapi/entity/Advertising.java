package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.enums.Advertisinompany;
import com.bg.myrefrigeratorapi.enums.PaymentType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Advertising {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private Advertisinompany advertisinompany;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private PaymentType paymentType;

    @Column(nullable = false)
    private Double paymentAmount;

    @Column(nullable = false)
    private LocalDate datePayment;

    @Column(nullable = false)
    private LocalDate contractStartDate;

    @Column(nullable = false)
    private LocalDate endDateOfContract;

    @Column(nullable = false)
    private Boolean isExpiration;

    @Column(columnDefinition = "TEXT")
    private String requested;
}

