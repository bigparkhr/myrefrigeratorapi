package com.bg.myrefrigeratorapi.model.answer;

import com.bg.myrefrigeratorapi.entity.Questions;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AnswerItem {
    private Long id;
    private String questionsId;
    private String AnswerTitle;
    private String Answer;
    private LocalDate dateAnswer;
}
