package com.bg.myrefrigeratorapi.model.answer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerStatusChangeRequest {
    private String AnswerTitle;
    private String Answer;
}
