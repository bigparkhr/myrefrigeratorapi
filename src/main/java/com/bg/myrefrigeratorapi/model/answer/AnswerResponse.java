package com.bg.myrefrigeratorapi.model.answer;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AnswerResponse {
    private Long id;
    private Long questionsId;
    private String AnswerTitle;
    private String Answer;
    private LocalDate dateAnswer;
    //private Boolean isAnswerDelete;
    //private LocalDate dateAnswerDelete;
}
