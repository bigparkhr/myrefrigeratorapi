package com.bg.myrefrigeratorapi.model.refrigeratorfood;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class RefrigeratorFoodRequest {
    private String refrigeratorFood;

    private LocalDate dateRefrigeratorFood;
    }

