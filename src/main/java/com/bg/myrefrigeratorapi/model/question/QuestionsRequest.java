package com.bg.myrefrigeratorapi.model.question;

import com.bg.myrefrigeratorapi.enums.QuestionCategory;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class QuestionsRequest {

    private QuestionCategory questionCategory;

    private String title;

    private String questionContents;

    private Boolean answerStatus;

    private Boolean isDelete;

    private LocalDate dateDelete;
}
