package com.bg.myrefrigeratorapi.model.question;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.enums.QuestionCategory;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class QuestionsItem {
    private Long id;
    private String memberEntityId;
    private QuestionCategory questionCategory;
    private String title;
    private String questionContents;
    private String answerStatus;
    private LocalDate dateWrite;
}
