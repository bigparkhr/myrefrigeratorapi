package com.bg.myrefrigeratorapi.model.question;

import com.bg.myrefrigeratorapi.enums.QuestionCategory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionStatusChangeRequest {
    private QuestionCategory questionCategory;
    private String title;
    private String questionContents;
    private Boolean answerStatus;
}
