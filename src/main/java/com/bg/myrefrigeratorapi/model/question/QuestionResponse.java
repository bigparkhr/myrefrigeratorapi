package com.bg.myrefrigeratorapi.model.question;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.enums.QuestionCategory;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class QuestionResponse {
    private Long id;
    private Member memberEntityId;
    private QuestionCategory questionCategory;
    private String title;
    private String questionContents;
    private String answerStatusName;
    private LocalDate dateWrite;
    private Boolean isDelete;
    private LocalDate dateDelete;
}
