package com.bg.myrefrigeratorapi.model.foodIngredient;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.bg.myrefrigeratorapi.enums.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodIngredientItem {
    private Long id;
    private Long foodName;
    private Category category;
    private Long refrigeratorFood;
    private String capacity;
}
