package com.bg.myrefrigeratorapi.model.foodIngredient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodIngredientSearch {
    private Long foodNameId;
    private String foodName;
    private String refrigeratorFood;
}
