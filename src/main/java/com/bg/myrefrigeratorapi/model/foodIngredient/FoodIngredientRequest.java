package com.bg.myrefrigeratorapi.model.foodIngredient;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.bg.myrefrigeratorapi.enums.Category;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodIngredientRequest {
    @Enumerated(value = EnumType.STRING)
    private Category category;
    private RefrigeratorFood refrigeratorFood;
    private String capacity;
}
