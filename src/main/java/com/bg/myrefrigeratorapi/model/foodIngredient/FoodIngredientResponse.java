package com.bg.myrefrigeratorapi.model.foodIngredient;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodIngredientResponse {
    private String refrigeratorFood;
}
