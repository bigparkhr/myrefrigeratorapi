package com.bg.myrefrigeratorapi.model.comment;

import com.bg.myrefrigeratorapi.entity.Comment;
import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentDTO {
    private Long id;
    private Member member;
    private String commentContents;
    private FoodName foodName;
    private LocalDateTime commentCreatedTime;

    public static CommentDTO toCommentDTO(Comment comment, FoodName foodNameId) {
        CommentDTO commentDTO = new CommentDTO();

        commentDTO.setId(comment.getId());
        commentDTO.setMember(commentDTO.getMember());
        commentDTO.setCommentContents(commentDTO.getCommentContents());
        commentDTO.setCommentCreatedTime(commentDTO.getCommentCreatedTime());
        commentDTO.setFoodName(foodNameId);

        return commentDTO;
    }
}
