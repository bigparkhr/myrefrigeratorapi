package com.bg.myrefrigeratorapi.model.comment;

import com.bg.myrefrigeratorapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentRequest {
    private String commentContents;
    //private LocalDateTime commentCreatedTime;
}
