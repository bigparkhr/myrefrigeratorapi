package com.bg.myrefrigeratorapi.model.comment;

import com.bg.myrefrigeratorapi.entity.FoodName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentItem {
    private Long id;
    private String foodName;
    private String memberId;
    private String commentContents;
    private LocalDateTime commentCreatedTime;
}
