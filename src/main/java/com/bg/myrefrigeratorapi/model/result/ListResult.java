package com.bg.myrefrigeratorapi.model.result;

import com.bg.myrefrigeratorapi.model.result.CommonResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list;
    private Long totalCount;
}
