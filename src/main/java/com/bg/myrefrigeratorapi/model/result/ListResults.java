package com.bg.myrefrigeratorapi.model.result;

import com.bg.myrefrigeratorapi.model.result.CommonResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResults<T> extends CommonResult {
    private String msg;
    private Integer code;
    private List<T> list;
    private Long totalCount; // 총 데이터개수
    private Integer totalPage; // 총 페이지수 -> 1 페이지
    private Integer currentPage; // 현재 페이지번호 -1

}
