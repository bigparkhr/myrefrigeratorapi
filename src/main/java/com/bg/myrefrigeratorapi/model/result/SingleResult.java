package com.bg.myrefrigeratorapi.model.result;

import com.bg.myrefrigeratorapi.model.result.CommonResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private String msg;
    private Integer code;
    private T data;

}
