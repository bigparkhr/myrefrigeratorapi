package com.bg.myrefrigeratorapi.model.login;

import com.bg.myrefrigeratorapi.interfaces.CommonModelBuilder;
import lombok.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private String token;
    private String name;

    private LoginResponse(Builder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public Builder(String token, String username) {
            this.token = token;
            this.name =  username;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
