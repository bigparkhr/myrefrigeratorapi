package com.bg.myrefrigeratorapi.model.refrigerator;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class RefrigeratorRequest {

   private RefrigeratorFood refrigeratorFood;

   private LocalDate dateRefrigerator;

   private String RefrigeratorName;
}
