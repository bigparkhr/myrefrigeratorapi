package com.bg.myrefrigeratorapi.model.refrigerator;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefrigeratorStatusChangeRequest {
    private RefrigeratorFood refrigeratorFood;
}
