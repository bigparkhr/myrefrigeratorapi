package com.bg.myrefrigeratorapi.model.refrigerator;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RefrigeratorItem {
    private Long id;
    private Long memberId;
    private String refrigeratorFood;
    private LocalDate dateRefrigerator;
    private String RefrigeratorName;
}
