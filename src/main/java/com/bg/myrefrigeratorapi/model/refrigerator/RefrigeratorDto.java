package com.bg.myrefrigeratorapi.model.refrigerator;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.fasterxml.jackson.databind.util.BeanUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;

@Builder
@Getter
@AllArgsConstructor
public class RefrigeratorDto {
    private Long id;
    private String refrigeratorFood;
    private LocalDate dateRefrigeratorFood;

    //public RefrigeratorDto(RefrigeratorFood refrigeratorFood) {
        //BeanUtils.copyProperties(refrigeratorFood, this);
    }

