package com.bg.myrefrigeratorapi.model.advertising;

import com.bg.myrefrigeratorapi.enums.Advertisinompany;
import com.bg.myrefrigeratorapi.enums.PaymentType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class AdvertisingRequest {
    private Advertisinompany advertisinompany;
    private PaymentType paymentType;
    private Double paymentAmount;
    private LocalDate datePayment;
    private LocalDate contractStartDate;
    private LocalDate endDateOfContract;
    private Boolean isExpiration;
    private String requested;
}

