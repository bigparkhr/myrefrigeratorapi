package com.bg.myrefrigeratorapi.model.advertising;

import com.bg.myrefrigeratorapi.enums.Advertisinompany;
import com.bg.myrefrigeratorapi.enums.PaymentType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AdvertisingStatusChangeRequest {
    private Advertisinompany advertisinompany;
    private PaymentType paymentType;
    private LocalDate endDateOfContract;
    private String requested;
}
