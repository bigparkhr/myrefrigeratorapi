package com.bg.myrefrigeratorapi.model.orderofcooking;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderOfCookingRequest {
    private String recipe;
}
