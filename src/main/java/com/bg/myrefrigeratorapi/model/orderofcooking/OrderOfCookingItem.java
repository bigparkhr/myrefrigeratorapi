package com.bg.myrefrigeratorapi.model.orderofcooking;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderOfCookingItem {
    private Long id;
    private Long foodNameId;
    private String recipe;
}
