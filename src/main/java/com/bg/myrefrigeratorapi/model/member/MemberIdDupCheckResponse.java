package com.bg.myrefrigeratorapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberIdDupCheckResponse {
    private Boolean isNew;
}
