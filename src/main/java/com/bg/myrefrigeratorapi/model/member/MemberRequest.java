package com.bg.myrefrigeratorapi.model.member;

import com.bg.myrefrigeratorapi.enums.InterLockInfo;
import com.bg.myrefrigeratorapi.enums.MemberGroup;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    private String username;

    @NotNull
    private String memberId;

    @NotNull
    private InterLockInfo interlockInfo;

    @NotNull
    private String password;

    @NotNull
    private String passwordRe;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String mail;

    @NotNull
    private LocalDate dateJoin;

    @NotNull
    private Boolean isOut;

    private LocalDate dateOut;

    @NotNull
    private MemberGroup memberGroup;
}
