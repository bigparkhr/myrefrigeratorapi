package com.bg.myrefrigeratorapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemeberStatusChangeRequest {
    private String memberName;
    private String memberId;
    private String password;
    private String phoneNumber;
    private String mail;
    private LocalDate dateJoin;
}
