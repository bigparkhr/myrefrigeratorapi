package com.bg.myrefrigeratorapi.model.member;

import com.bg.myrefrigeratorapi.enums.InterLockInfo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class MemberItem {

    private Long id;

    private String memberName;

    private String memberId;

    private InterLockInfo interlockInfo;

    private String phoneNumber;

    private String mail;

    private LocalDate dateJoin;

}
