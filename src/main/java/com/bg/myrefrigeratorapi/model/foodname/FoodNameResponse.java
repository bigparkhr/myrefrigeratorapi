package com.bg.myrefrigeratorapi.model.foodname;

import com.bg.myrefrigeratorapi.entity.Refrigerator;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FoodNameResponse {
    private Long id;
    private String foodName;
    private String recipeImage;
    private String calorie;
    private LocalDate dateRegister;
    private Boolean isRecipeDelete;
    private LocalDate dateRecipeDelete;
}
