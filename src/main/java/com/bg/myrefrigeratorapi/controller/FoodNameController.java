package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameItem;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameRequest;
import com.bg.myrefrigeratorapi.model.foodname.FoodNameResponse;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/food-name")
public class FoodNameController {
    private final FoodNameService foodNameService;

    @PostMapping("/new")
    @Operation(summary = "음식 정보 등록")
    public CommonResult setFoodName(@RequestBody FoodNameRequest request) {
        foodNameService.setFoodName(request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all")
    @Operation(summary = "음식 정보 목록")
    public ListResult<FoodNameItem> getFoodNames() {
        //List<FoodNameItem> foodNames = foodNameService.getFoodNames();
        ListResult<FoodNameItem> result = new ListResult<>();
        return ResponseService.getListResult(result, true);
    }


    @GetMapping("/detail/{id}")
    @Operation(summary = "음식 정보 상세보기")
    public SingleResult<FoodNameResponse> getFoodName(@PathVariable long id) {
        return ResponseService.getSingleResult(foodNameService.getFoodName(id));
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "음식 정보 Paging")
    public ListResults<FoodName> getFoodNames(@PathVariable int pageNum) {
        return foodNameService.getFoodNames(pageNum);
    }
}
