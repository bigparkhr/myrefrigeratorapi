package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Advertising;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingItem;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingRequest;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingResponse;
import com.bg.myrefrigeratorapi.model.advertising.AdvertisingStatusChangeRequest;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.service.AdvertisingService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/Advertising")
public class AdvertisingController {
    private final AdvertisingService advertisingService;

    @PostMapping("/Payment")
    @Operation(summary = "광고 정보 등록")
    public CommonResult setAdvertising(@RequestBody AdvertisingRequest request){
        advertisingService.setAdvertising(request);

        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "광고 정보 목록")
    public ListResult<AdvertisingItem> getAdvertisings() {
        return ResponseService.getListResult(advertisingService.getAdvertisings());
    }*/
    @GetMapping("/detail/{id}")
    @Operation(summary = "광고 정보 상세보기")
    public SingleResult<AdvertisingResponse> getAdvertising(@PathVariable long id) {
        advertisingService.getAdvertising(id);

        return ResponseService.getSingleResult(advertisingService.getAdvertising(id));
    }
    @PutMapping("/status/{id}")
    @Operation(summary = "광고 정보 수정")
    public CommonResult putAdvertising(@PathVariable long id, @RequestBody AdvertisingStatusChangeRequest request) {
        advertisingService.putAdvertising(id, request);

        return ResponseService.getSuccessResult();
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "광고 정보 Paging")
    public ListResults<Advertising> getAdvertisings(@PathVariable int pageNum) {
        return advertisingService.getAdvertisings(pageNum);
    }
}

