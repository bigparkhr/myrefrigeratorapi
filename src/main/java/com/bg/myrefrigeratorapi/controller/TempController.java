package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.service.TempService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    /*@PostMapping("/file-upload")
    public CommonResult setRentCarByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        tempService.setRentCarByFile(csvFile);
        return ResponseService.getSuccessResult();
    }*/
}
