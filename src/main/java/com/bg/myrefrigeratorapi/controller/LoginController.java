package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.enums.MemberGroup;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.model.login.LoginRequest;
import com.bg.myrefrigeratorapi.model.login.LoginResponse;
import com.bg.myrefrigeratorapi.service.LoginService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/web/admin")
    @Operation(summary = "로그인 기능")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_MEMBER, loginRequest, "APP"));
    }

    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_CEO, loginRequest, "APP"));
    }
}
