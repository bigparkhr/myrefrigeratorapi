package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.model.question.QuestionResponse;
import com.bg.myrefrigeratorapi.model.question.QuestionStatusChangeRequest;
import com.bg.myrefrigeratorapi.model.question.QuestionsItem;
import com.bg.myrefrigeratorapi.model.question.QuestionsRequest;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.QuestionsService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/questions")
@RequiredArgsConstructor
public class QuestionsController {
    private final QuestionsService questionsService;
    private final MemberService memberService;

    @PostMapping("/new/{memberEntityId}")
    @Operation(summary = "문의사항 등록")
    public CommonResult setQuestion(@PathVariable long memberEntityId, @RequestBody QuestionsRequest request) {
        Member member = memberService.getData(memberEntityId);
        questionsService.setQuestion(member, request);
        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "문의사항 목록")
    public ListResult<QuestionsItem> getQuestions() {
        return ResponseService.getListResult(questionsService.getQuestions());
    }*/
    @GetMapping("/detail/{id}")
    @Operation(summary = "문의사항 상세보기")
    public SingleResult<QuestionResponse> getQuestion(@PathVariable long id) {
        return ResponseService.getSingleResult(questionsService.getQuestion(id));
    }
    @PutMapping("/status/{id}")
    @Operation(summary = "문의사항 수정")
    public CommonResult putQuestionStatus(@PathVariable long id, @RequestBody QuestionStatusChangeRequest request) {
        questionsService.putQuestionStatus(id, request);

        return ResponseService.getSuccessResult();
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "문의사항 삭제")
    public CommonResult delQuestion(@PathVariable long id) {
        questionsService.delQuestion(id);

        return ResponseService.getSuccessResult();
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의사항 Paging")
    public ListResults<Questions> getQuestions(@PathVariable int pageNum) {
        return questionsService.getQuestions(pageNum);
    }
}
