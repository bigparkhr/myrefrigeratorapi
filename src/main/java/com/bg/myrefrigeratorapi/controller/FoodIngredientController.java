package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientItem;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientRequest;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientResponse;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientSearch;
import com.bg.myrefrigeratorapi.service.FoodIngredientService;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/food-ingredient")
public class FoodIngredientController {
    private final FoodIngredientService foodIngredientService;
    private final FoodNameService foodNameService;

    @PostMapping("new/food-name-id/{foodId}")
    @Operation(summary = "음식 성분 등록")
    public CommonResult setFoodIngredient(@PathVariable long foodId, @RequestBody FoodIngredientRequest request) {
        FoodName foodName = foodNameService.getData(foodId);
        foodIngredientService.setFoodIngredient(foodName, request);

        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "음식 성분 목록")
    public ListResult<FoodIngredientItem> getFoodIngredients() {
        return ResponseService.getListResult(foodIngredientService.getFoodIngredients());
    }*/

    /**
     *
     * @param refrigeratorId
     * @return 식재료를 통한 검색 기능
     */
    /*@GetMapping("/refrigerator-food-id/{refrigeratorId}")
    @Operation(summary = "식재료를 통한 검색 기능")
    private ListResult<FoodIngredientSearch> getSearch(@PathVariable long refrigeratorId) {
        return ResponseService.getListResult(foodIngredientService.getSearch(refrigeratorId));
    }*/

    /**
     * 음식에 대한 재료 목록 불러오기
     * @param foodNameId
     * @return
     */
    /*@GetMapping("/food-name-id/{foodNameId}")
    @Operation(summary = "음식에 대한 재료 목록")
    private ListResult<FoodIngredientResponse> getFoodIngredient(@PathVariable long foodNameId) {
        FoodName foodName = foodNameService.getData(foodNameId);
        return ResponseService.getListResult(foodIngredientService.getFoodIngredient(foodName));
    }*/
}