package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorItem;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorRequest;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorStatusChangeRequest;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.RefrigeratorService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("v1/refrigerator")
public class RefrigeratorController {
    private final MemberService memberService;
    private final RefrigeratorService refrigeratorService;

    @PostMapping("/new/member-id/{memberId}")
    @Operation(summary = "회원 id와 식재료 id를 받아서 냉장고 재료 등록")
    public CommonResult setRefrigerator(@PathVariable long memberId, @RequestBody RefrigeratorRequest request) {
        Member member = memberService.getData(memberId);
        refrigeratorService.setRefrigerator(member, request);

        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "냉장고 재료 목록 보기")
    public ListResult<RefrigeratorItem> getRefrigerators() {
        return ResponseService.getListResult(refrigeratorService.getRefrigerators());
    }*/
    /*@GetMapping("/all/member-id/{memberId}")
    @Operation(summary = "회원 id로 식재료 정보 불러오기")
    public ListResult<RefrigeratorItem> getRefrigerator(@PathVariable long memberId) {
        Member member = memberService.getData(memberId);
        return ResponseService.getListResult(refrigeratorService.getRefrigerator(member));
    }*/
    @PutMapping("/status/{id}")
    @Operation(summary = "냉장고 재료 정보 수정")
    public CommonResult putRefrigerator(@PathVariable long id, @RequestBody RefrigeratorStatusChangeRequest request) {
        refrigeratorService.putRefrigerator(id, request);

        return ResponseService.getSuccessResult();
    }
    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "냉장고 재료 Paging")
    public ListResults<Refrigerator> getRefrigerators(@PathVariable int pageNum) {
        return refrigeratorService.getRefrigerators(pageNum);
    }
}