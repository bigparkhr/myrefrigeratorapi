package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Answer;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.model.answer.AnswerItem;
import com.bg.myrefrigeratorapi.model.answer.AnswerRequest;
import com.bg.myrefrigeratorapi.model.answer.AnswerResponse;
import com.bg.myrefrigeratorapi.model.answer.AnswerStatusChangeRequest;
import com.bg.myrefrigeratorapi.service.AnswerService;
import com.bg.myrefrigeratorapi.service.QuestionsService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/answer")
public class AnswerController {
    private final AnswerService answerService;
    private final QuestionsService questionsService;

    @PostMapping("/new/{questionsId}")
    @Operation(summary = "답변 등록")
    public CommonResult setQuestion(@PathVariable long questionsId, @RequestBody AnswerRequest request){
        Questions questions = questionsService.getDataIs(questionsId);
        answerService.setAnswer( questions , request);

        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "답변 목록")
    public ListResult<AnswerItem> getAnswers() {
        return ResponseService.getListResult(answerService.getAnswers());
    }*/
    @GetMapping("/detail/{id}")
    @Operation(summary = "답변 상세보기")
    public SingleResult<AnswerResponse> getAnswer(@PathVariable long id) {
        return ResponseService.getSingleResult(answerService.getAnswer(id));
    }
    @PutMapping("/status/{id}")
    @Operation(summary = "답변 수정")
    public CommonResult putAnswer(@PathVariable long id, @RequestBody AnswerStatusChangeRequest request) {
        answerService.putAnswer(id, request);

        return ResponseService.getSuccessResult();
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "답변 삭제")
    public CommonResult delAnswer(@PathVariable long id) {
        answerService.delAnswer(id);

        return ResponseService.getSuccessResult();
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "답변 Paging")
    public ListResults<Answer> getAnswers(@PathVariable int pageNum) {
        return answerService.getAnswers(pageNum);
    }
}
