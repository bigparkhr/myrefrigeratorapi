package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingChangeRequest;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingItem;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingRequest;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.OrderOfCookingService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/order-of-cooking")
public class OrderOfCookingController {
    private final OrderOfCookingService orderOfCookingService;
    private final FoodNameService foodNameService;

   @PostMapping("/new/food-name-id/{foodNameId}")
   @Operation(summary = "레시피 등록")
    public CommonResult setOrderOfCooking(@PathVariable long foodNameId, @RequestBody OrderOfCookingRequest request) {
        FoodName foodName = foodNameService.getData(foodNameId);
       orderOfCookingService.setOrderOfCooking(foodName, request);

        return ResponseService.getSuccessResult();
    }
    /*@GetMapping("/all")
    @Operation(summary = "레시피 목록")
    public ListResult<OrderOfCookingItem> getOrderOfCookings() {
        return ResponseService.getListResult(orderOfCookingService.getOrderOfCookings());
    }*/
    /*@GetMapping("/all/{foodNameId}")
    @Operation(summary = "레시피 상세보기")
    public ListResult<OrderOfCookingItem> getFoodNameOrderOfCookings(@PathVariable long foodNameId) {
        FoodName foodName = foodNameService.getData(foodNameId);
        return ResponseService.getListResult(orderOfCookingService.getFoodNameOrderOfCookings(foodName));
    }*/
    @PutMapping("/status/{id}")
    @Operation(summary = "레시피 수정")
    public CommonResult putOrderOfCooking(@PathVariable long id, @RequestBody OrderOfCookingChangeRequest request) {
        orderOfCookingService.putOrderOfCooking(id, request);

        return ResponseService.getSuccessResult();
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "레시피 삭제")
    public CommonResult delOrderOFCooking(@PathVariable long id) {
        orderOfCookingService.delOrderOFCooking(id);
        return ResponseService.getSuccessResult();
    }
}
