package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.enums.MemberGroup;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.result.SingleResult;
import com.bg.myrefrigeratorapi.model.member.*;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/member")
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @GetMapping("/memberId/check")
    @Operation(summary = "회원가입 할때 id 중복 검사")
    public SingleResult<MemberIdDupCheckResponse> getDupCheck(@RequestParam(name = "memberId") String memberId) {
        return ResponseService.getSingleResult(memberService.getDupCheck(memberId));
    }
    @PostMapping("/new/user")
    @Operation(summary = "회원 등록")
    public CommonResult createUser(@RequestBody @Valid MemberRequest request)   {
        MemberGroup memberGroup = MemberGroup.ROLE_MEMBER;
        memberService.createMember(memberGroup, request);
        return ResponseService.getSuccessResult();
    }

    @PostMapping("/new/admin")
    public CommonResult createAdmin(@RequestBody @Valid MemberRequest request) {
        MemberGroup memberGroup = MemberGroup.ROLE_CEO;
        memberService.createMember(memberGroup, request);
        return ResponseService.getSuccessResult();
    }

    /*@GetMapping("/all")
    @Operation(summary = "회원 정보 조회")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers());
    }*/

   /* @GetMapping("/detail/{id}")
    @Operation(summary = "회원 정보 상세보기")
    public SingleResult<MemberResponse> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }*/
    /*@PutMapping("/status/{id}")
    @Operation(summary = "회원 정보 수정")
    public CommonResult putMemberStatus(@PathVariable long id, @RequestBody MemeberStatusChangeRequest request) {
        memberService.putMemberStatus(id, request);
        return ResponseService.getSuccessResult();
    }*/
   /* @PutMapping("/out/{id}")
    @Operation(summary = "회원 정보 삭제")
    public CommonResult putMemberOut(@PathVariable long id) {
        memberService.putMemberOut(id);
        return ResponseService.getSuccessResult();
    }*/
    /**
     * Paging
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "회원 정보 Paging")
    public ListResults<Member> getMembers(@PathVariable int pageNum) {
        return memberService.getMembers(pageNum);
    }
}
//ResponseService.getListResult(memberService.getMembers(pageNum).getList());
//memberService.getMembers(pageNum);