package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.comment.CommentItem;
import com.bg.myrefrigeratorapi.model.result.ListResult;
import com.bg.myrefrigeratorapi.model.comment.CommentRequest;
import com.bg.myrefrigeratorapi.service.CommentService;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentController {
    private final CommentService commentService;
    private final FoodNameService foodNameService;
    private final MemberService memberService;

    /**
     * 레시피에 댓글 등록 기능
     * @param foodNameId
     * @param memberNameId
     * @param request
     * @return
     */
  /*  @PostMapping("/new/food-name-id/{foodNameId}/member-name-id/{memberNameId}")
    @Operation(summary = "댓글 등록")
    public CommonResult setComment(@PathVariable long foodNameId, @PathVariable long memberNameId, CommentRequest request) {
        FoodName foodName = foodNameService.getData(foodNameId);
        Member member = memberService.getData(memberNameId);
        commentService.setComment(foodName, member, request);

        return ResponseService.getSuccessResult();
    }*/
    /**
     *
     * @param foodNameId
     * @return 댓글 리스틑 가져오는 기능
     */
    /*@GetMapping("/all/{foodNameId}")
    @Operation(summary = "댓글 리스트 조회")
    public ListResult<CommentItem> getComments(@PathVariable long foodNameId) {
        FoodName foodName1 = foodNameService.getData(foodNameId);
        return ResponseService.getListResult(commentService.getComments(foodName1));
    }*/
}
