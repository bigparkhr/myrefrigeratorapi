package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.exception.CRefrigeratorFullException;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CRefrigeratorFullException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPontException() {
        throw new CRefrigeratorFullException();
    }
}
