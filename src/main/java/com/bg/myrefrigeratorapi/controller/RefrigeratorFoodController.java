package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.bg.myrefrigeratorapi.model.result.CommonResult;
import com.bg.myrefrigeratorapi.model.result.ListResults;
import com.bg.myrefrigeratorapi.model.refrigeratorfood.RefrigeratorFoodRequest;
import com.bg.myrefrigeratorapi.service.RefrigeratorFoodService;
import com.bg.myrefrigeratorapi.service.response.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/refrigerator_food")
public class RefrigeratorFoodController {
    private final RefrigeratorFoodService refrigeratorFoodService;

    @PostMapping("/new")
    @Operation(summary = "식재료 등록")
    public CommonResult setRefrigeratorFood(@RequestBody RefrigeratorFoodRequest request){
        refrigeratorFoodService.setRefrigeratorFood(request);
        return ResponseService.getSuccessResult();
    }

    /**
     * Paging
     * @param pageNum
     * @return
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "식재료 정보 Paging")
    public ListResults<RefrigeratorFood> getRefrigerators(@PathVariable int pageNum) {
        return refrigeratorFoodService.getRefrigerators(pageNum);
    }
}
