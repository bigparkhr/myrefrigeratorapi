package com.bg.myrefrigeratorapi.lib;

public class ComonCheck {

    public static boolean checkUsername(String username) {
        String pattern = "^[a-zA-Z]{1}[a-xA-Z0-9]{4,19}$";
        return username.matches(pattern);
    }
}
