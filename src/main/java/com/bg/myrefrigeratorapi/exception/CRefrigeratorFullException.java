package com.bg.myrefrigeratorapi.exception;

import com.bg.myrefrigeratorapi.enums.code.ResultCode;

public class CRefrigeratorFullException extends RuntimeException {
    public CRefrigeratorFullException(String msg, Throwable t) {
        super(msg, t);
    }
    public CRefrigeratorFullException(String msg) {
        super(msg);
    }
    public CRefrigeratorFullException() {
        String msg = ResultCode.CHANGE.getMsg();
    }
}
