package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
