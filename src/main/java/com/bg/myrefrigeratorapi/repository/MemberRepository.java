package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByMemberIdOrderByIdDesc(String memberId);
    //Optional<Member> findByMemberIdAndPassword(String memberId, String password);
    List<Member> findAllByOrderByIdAsc();

    Optional<Member> findByUsername(String username);
}
