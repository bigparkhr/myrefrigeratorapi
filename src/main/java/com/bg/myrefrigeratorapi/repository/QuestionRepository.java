package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Questions, Long> {
    List<Questions> findAllByOrderByIdAsc();
}
