package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RefrigeratorFoodRepository extends JpaRepository<RefrigeratorFood, Long> {
}
