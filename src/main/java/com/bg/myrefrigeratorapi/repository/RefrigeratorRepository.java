package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RefrigeratorRepository extends JpaRepository <Refrigerator, Long > {
    //List<Refrigerator> findAllByMember(Member member);
    List<Refrigerator> findByMember(Member member);
}
