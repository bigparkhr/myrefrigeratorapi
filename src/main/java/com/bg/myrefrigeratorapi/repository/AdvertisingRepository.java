package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Advertising;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdvertisingRepository extends JpaRepository  <Advertising,Long> {
}

