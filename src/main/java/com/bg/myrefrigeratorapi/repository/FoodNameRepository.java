package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.FoodName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FoodNameRepository extends JpaRepository<FoodName, Long> {
    //Optional<FoodName> findById(FoodName foodName);
}
