package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Comment;
import com.bg.myrefrigeratorapi.entity.FoodName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    //List<Comment> findAllByFoodNameOrderByIdDesc(FoodName foodName);
    //List<Comment> findByFoodName_Id(Long id);

    List<Comment> findAllByFoodNameOrderByIdAsc(FoodName foodName);
}
