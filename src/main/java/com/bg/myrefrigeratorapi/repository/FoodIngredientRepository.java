package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.FoodIngredient;
import com.bg.myrefrigeratorapi.entity.FoodName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FoodIngredientRepository extends JpaRepository<FoodIngredient, Long> {
     List<FoodIngredient> findAllByRefrigeratorFoodId(long refrigeratorFood);
     List<FoodIngredient> findByFoodName(FoodName foodName);
}
