package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.OrderOfCooking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderOfCookingRepository extends JpaRepository<OrderOfCooking, Long> {
    List<OrderOfCooking> findAllByFoodNameIdOrderByIdAsc(FoodName foodName);
}
